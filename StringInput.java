import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class StringInput {

	private Scanner in;

	public StringInput() {
		in = new Scanner(System.in);
		startLoop();
	}

	private void startLoop() {
            
            boolean end = false;
            while(!end)
            {
		String word = getWordFromUserInput();

		boolean quit = false;
                while(!quit)
                {
			showOptions(word);
			String userInput = getUserSelectedOption();

			int option = validateUserInput(userInput);
			if (option == 8) { //option to quit
				quit = true;
			} else if(option != -1) {
                                //strings are immutable and passed by value, so original word won't be affected
				executeUserSelectedOption(option, word);
			}
                        else
                        {
                            System.out.println("Wrong input!");
                        }
                }
                end = !askUserIfContinue();
            }
            //the end.
            System.out.println("Bye.");
            end();
	}


	private void end() {
		in.close();
	}

	private boolean askUserIfContinue() {
		System.out.print("Do you want to enter another word? Yes/No: ");
		String s = getUserInput();
		if (s.trim().toLowerCase().startsWith("y")) {
			return true;
		} else {
			return false;
		}
	}

	private String getWordFromUserInput() {
		System.out.print("Enter a word: ");
		String s = getUserInput();
		System.out.println();

		return s.trim();
	}

	private String getUserInput() {
		return in.nextLine();
	}

	private void showOptions(String word) {
		System.out.println("Current word: '" + word + "'");
		System.out.println("Choose one of the following options:");
		System.out.println("1)\tRepeat");
		System.out.println("2)\tReverse");
		System.out.println("3)\tArrange Alphabetically");
		System.out.println("4)\tRandomise");
		System.out.println("5)\tFrequency Count");
		System.out.println("6)\tRemove Vowels");
		System.out.println("7)\tInsert Spaces");
		System.out.println("8)\tQuit");
		System.out.println();
	}

	private String getUserSelectedOption() {
		System.out.print("Enter your choice: ");
		String s = getUserInput();
		System.out.println();
		return s;
	}

	private int validateUserInput(String data) {
		if (data == null || data.trim().length() == 0) {
			return -1;
		}

		String possibleNumber = data.trim();
		if (possibleNumber.length() > 1) { //change the 1 to 2 if we go over 9 options (change again to 3 if we go over 99 options...)
			return -1;
		}

		if (possibleNumber.equals("1")) return 1;
		if (possibleNumber.equals("2")) return 2;
		if (possibleNumber.equals("3")) return 3;
		if (possibleNumber.equals("4")) return 4;
		if (possibleNumber.equals("5")) return 5;
		if (possibleNumber.equals("6")) return 6;
		if (possibleNumber.equals("7")) return 7;
		if (possibleNumber.equals("8")) return 8;

		return -1;
	}

	private void executeUserSelectedOption(int option, String word) {
		if (option < 1) {
			System.out.println("Unknown option.");
			return;
		}

		switch (option) {
			case 1: echo(word); break;
			case 2: reverse(word); break;
			case 3: alphabetise(word); break;
			case 4: randomise(word); break;
			case 5: frequency(word); break;
			case 6: removeVowels(word); break;
			case 7: insertSpaces(word); break;

			default: System.out.print("Error executing option.");
		}
		System.out.println();
	}


	private void echo(String word) {
		System.out.println(word);
	}
        
        private void reverse(String word) {
                if(word.length() > 0)
                {
                    for(int i = word.length()-1; i >= 0; i--)
                        System.out.print(word.charAt(i));
                }
                System.out.println();            
	}
        
        private void charCIBubbleSort(char[] array)
        {
            char buffer;
            for(int i = 0;i < array.length-1; i++)
            {
                for(int j = 0; j < array.length-i-1; j++)
                {
                    if(Character.toLowerCase(array[j]) > Character.toLowerCase(array[j+1]))
                    {
                        buffer = array[j];
                        array[j] = array[j+1];
                        array[j+1] = buffer;
                    }
                }
            }
        }
        
	private void alphabetise(String word) {
		char[] array = word.toCharArray();
		charCIBubbleSort(array);
                System.out.println(array);
	}

        
        private void randomise(String word)
        {
                char[] array = word.toCharArray();
                Random rand = new Random();
                for(int i = 0; i < array.length;i++)
                {
                    int index = rand.nextInt(array.length - i);
                    System.out.print(array[index]);
                    //shift array to exclude printed char
                    for(int j = index; j < array.length - i - 1;j++)
                        array[j] = array[j+1];
                }
                 System.out.println();
        }
        
        //Inner loop condition is j < length, so j will never be equal to lenght
        //And last iteration happens when j is equal to length-1
	private void frequency(String word) {
		word = word.toUpperCase();
		char[] array = word.toCharArray();
		int count;
		int length = array.length;

		for (int i = 0; i < length; i++) {
			count = 0;

			for (int j = 0; j < length; j++) {
				if (j < i && array[i] == array[j]) { //if we're looking at a character we've already seen (and already reported the frequency for)
					break; //break out of the inner loop so as not to re-report it! (de-duplication step)
				}

				if (array[i] == array[j]) {
					count++;
				}

				if (j == length-1) {
					System.out.println(array[i]+"\t"+count); //output the count on the last iteration of the inner loop
				}
			}
		}
	}
        
        
        private boolean isVowel(char ch)
        {
            char[] vowels = new char[]{'A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'};
            for(int i = 0; i < vowels.length; i++)
                if(ch == vowels[i])
                    return true;
            return false;
        }

        private void removeVowels(String word)
        {
            for(int i = 0; i < word.length();i++)
            {
                if(!isVowel(word.charAt(i)))
                    System.out.print(word.charAt(i)); 
            }
            System.out.println();
        }

	private void insertSpaces(String word) {
		int length = word.length() - 1;
		String s = "";
		char c;
		for (int i = 0; i < length; i++) {
			c = word.charAt(i);
			s += c + " ";
		}
		//do not add a space after the last character
		c = word.charAt(length);
		s += c;

		System.out.println(s);
	}

	public static void main(String[] args) {
		new StringInput();
	}
}
